<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('register', 'Auth\RegisterController@register'); // rota desativada
Route::post('register', 'Auth\RegisterController@postRegister'); // rota desativada

Route::get('criar-senha/{token}/{login}', ['as' => 'criar-senha', 'uses' => 'Auth\LoginController@getCriarSenha']);
Route::post('criar-senha', ['as' => 'gravar-senha', 'uses' => 'Auth\LoginController@postCriarSenha']);

Route::get('/', ['as' => 'home', 'uses' => 'CommonController@home']);

Route::get('termos-de-uso', function(){ return view('common.termos-de-uso'); });
Route::get('aviso-de-privacidade', function(){ return view('common.politica-de-privacidade'); });

Route::group([
'middleware' => ['auth.ativos', 'acesso_padrao']
], function(){
  Route::get('como-funciona', ['as' => 'como-funciona', 'uses' => 'CommonController@comoFunciona']);
  Route::get('regulamento', ['as' => 'regulamento', 'uses' => 'CommonController@regulamento']);
  Route::get('cronograma', ['as' => 'cronograma', 'uses' => 'CommonController@cronograma']);
  Route::get('avaliadores', ['as' => 'avaliadores', 'uses' => 'CommonController@avaliadores']);
  Route::get('consent-form', ['as' => 'user.consent', 'uses' => 'UserConsentController@form']);
  Route::post('consent-form', ['as' => 'user.consent.submit', 'uses' => 'UserConsentController@submit']);
});

Route::group([
'middleware' => ['auth.ativos', 'acesso_medico_coordenador']
], function(){
  Route::get('submeter-caso', ['as' => 'submeter-caso', 'uses' => 'CasosController@submeter']);
  Route::post('submeter-caso', ['as' => 'submeter-caso', 'uses' => 'CasosController@postSubmeter']);
  Route::get('download-formulario', ['as' => 'download-formulario', 'uses' => 'CasosController@downloadFormulario']);
});

Route::group([
'middleware' => ['auth.ativos', 'acesso_avaliador']
], function(){
  Route::get('avaliacoes', ['as' => 'avaliacoes', 'uses' => 'AvaliacoesController@index']);
  Route::get('buscar-avaliacoes', ['as' => 'buscar-avaliacoes', 'uses' => 'AvaliacoesController@buscar']);
  Route::post('enviar-notas', ['as' => 'avaliar', 'uses' => 'AvaliacoesController@enviarNotas']);
});


Route::group([
'middleware' => ['auth.ativos', 'acesso_admin']
], function(){

  Route::get('novos-casos', ['as' => 'novos-casos', 'uses' => 'AdminController@novosCasos']);
  Route::get('download-caso/{caso_codigo}', ['as' => 'download-caso', 'uses' => 'AdminController@downloadCaso']);
  Route::get('distribuir-caso/{caso_codigo}', ['as' => 'distribuir-caso', 'uses' => 'AdminController@distribuirCaso']);
  Route::get('excluir-caso/{caso_codigo}', ['as' => 'excluir-caso', 'uses' => 'AdminController@excluirCaso']);
  Route::get('status-envios', ['as' => 'status-envios', 'uses' => 'AdminController@statusEnvios']);
  Route::get('historico', ['as' => 'historico', 'uses' => 'AdminController@historico']);
  Route::get('ranking', ['as' => 'ranking', 'uses' => 'AdminController@ranking']);

  Route::get('log-adesao', ['as' => 'usuarios-log-adesao', 'uses' => 'AdminController@logAdesaoAosTermos']);
  Route::get('usuarios', ['as' => 'usuarios-lista', 'uses' => 'AdminController@usuarios']);
  Route::get('ativar-usuario/{email}', 'AdminController@ativarUsuario');
  Route::get('iniciar-usuarios', 'AdminController@iniciarUsuarios');

  /*
  Route::get('enviar-todos-casos', function(){
    $casos = RM\Models\Caso::all();
    foreach($casos as $x => $caso){
      echo $caso->id.' enviado<br>';
      //  ENVIAR CASO
      $ids = $caso->categoria.$caso->coordenador->id.$caso->id;
      $cod = 'C'.str_pad($ids, 6, '0', STR_PAD_LEFT);
      $caso->codigo = $cod;
      $caso->autor = 'Nome Autor teste#'.$x;
      $caso->enviado_em = Carbon\Carbon::now();

      $filename = $cod.'_'.Date('dmYHis').'.pdf';
      $caso->arquivo = $filename;
      $caso->excluido_em = null;
      $caso->save();
    }
  });

  Route::get('distribuir-todos', function(){
    $casos = RM\Models\Caso::all();
    foreach($casos as $x => $caso){
      echo $caso->id.' distribuido<br>';

      // DISTRIBUIR CASO
      $avaliadores = RM\Models\User::avaliadores()
                                      ->filtrarLocal($caso->coordenador->cidade)
                                      ->inRandomOrder()
                                      ->limit(3)
                                      ->get();


      if(count($avaliadores) != 3)
        return die('Não foi possível distribuir o caso clínico. (Erro: Total de avaliadores insuficiente (3), selecionados:'.count($avaliadores).')');

      foreach ($avaliadores as $avaliador) {
        // Gerar 1 registro de Avaliacao para cada avaliador
        $avaliacao = new RM\Models\Avaliacao(['casos_id' => $caso->id]);
        $avaliador->avaliacoes()->save($avaliacao);
      }

      $caso->distribuido_em = date('Y-m-d H:i:s');
      $caso->arquivo = null;
      $caso->save();
    }
  });
  */
});
