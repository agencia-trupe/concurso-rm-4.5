<?php

namespace RM\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class UserConsent
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if (! Auth::user()->isAdmin && !Auth::user()->isConsentGiven ) {
        return redirect('consent-form');
      }

      return $next($request);
    }
}
