<?php

namespace RM\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommonController extends Controller
{

  public function __construct()
  {
    $this->middleware(['auth', 'auth.ativos', 'user_consent']);
  }

  public function home(){
    return view('common.home');
  }

  public function regulamento(){
    return view('common.regulamento');
  }

  public function cronograma(){
    $today = date('Y-m-d');

    $faseAtual = 1;

    if ($today >= '2018-02-02' && $today <= '2018-03-16') {
      $faseAtual = 2;
    } elseif($today >= '2018-03-17' && $today <= '2018-03-18') {
      $faseAtual = 3;
    } elseif($today >= '2018-03-19' && $today <= '2018-08-17') {
      $faseAtual = 4;
    } elseif($today >= '2018-08-18' && $today <= '2018-09-30') {
      $faseAtual = 5;
    } elseif($today >= '2018-09-30' && $today <= '2018-11-01') {
      $faseAtual = 6;
    } elseif($today > '2018-11-02') {
      $faseAtual = 7;
    }

    return view('common.cronograma', [
      'faseAtual' => $faseAtual
    ]);
  }

  public function avaliadores(){
    return view('common.avaliadores');
  }

  public function comoFunciona()
  {
    return view('common.como-funciona');
  }

}
