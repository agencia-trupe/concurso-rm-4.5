<?php

namespace RM\Http\Controllers;

use Auth;
use Carbon\Carbon as Carbon;
use Illuminate\Http\Request;

class UserConsentController extends Controller{

  public function __construct()
  {
    $this->middleware('auth');
  }

  public function form()
  {
    return view('auth.consent-form');
  }

  public function submit(Request $request)
  {
    $this->validate($request, [
      'check_termos' => 'required|in:1'
    ]);

    try {

      $user = \Auth::user();

      $user->check_termos = $request->check_termos;
      $user->check_termos_date = date('Y-m-d H:i:s');
      $user->save();

      return redirect('/');

    } catch (\Exception $e) {

      $erro = logar_erro($e, Auth::user()->id);
      return back()->withErrors(array('Não foi possível armazenar os dados de aceite. (Erro: '.$erro.')'));

    }
  }

}