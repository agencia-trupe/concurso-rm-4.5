<?php

namespace RM\Models;

use RM\Notifications\ResetarSenha;
use RM\Notifications\UsuarioCriado;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

  use Notifiable;

  protected $table = 'usuarios';

  protected $fillable = [
    'tipo',
    'nome',
    'crm',
    'is_ativo',
    'cidade',
    'estado',
    'email',
    'password',
    'check_termos',
    'check_termos_date'
  ];

  protected $hidden = [
    'password',
    'remember_token'
  ];

  protected $dates = [
    'senha_criada_em',
    'email_enviado_em',
    'check_termos_date',
    'created_at',
    'updated_at'
  ];

  /**
   * Send the password reset notification.
   *
   * @param  string  $token
   * @return void
   */
  public function sendPasswordResetNotification($token)
  {
    $this->notify(new ResetarSenha($token, $this->email));
  }

  public function sendPasswordCreationNotification($token)
  {
    $this->notify(new UsuarioCriado($token, $this->email));
  }

  // COMPUTED ATTRIBUTES

  public function getTipoExtensoAttribute()
  {
    return ucfirst($this->tipo);
  }

  public function getIsCoordenadorAttribute()
  {
    return $this->tipo == 'coordenador';
  }

  public function getIsAvaliadorAttribute()
  {
    return $this->tipo == 'avaliador';
  }

  public function getIsAdminAttribute()
  {
    return $this->tipo == 'admin';
  }

  public function getIsAtivoAttribute()
  {
    return !is_null($this->senha_criada_em);
  }

  public function getIsConsentGivenAttribute()
  {
    return $this->check_termos == 1 && !is_null($this->check_termos_date);
  }

  public function getPodeAvaliarAttribute($caso_id)
  {
    // verificar se tem uma avaliação pra esse avaliador e id de caso
    return sizeof($this->avaliacoes()->where('casos_id', $caso_id)->get()) > 0 ? true : false;
  }

  public function getStatusEnvioCaso1Attribute()
  {
    $caso = $this->casos()->categoria(1)->first();

    return is_null($caso->enviado_em) ?
            "<strong>NÃO ENVIADO</strong>" :
            "ENVIADO EM: ".$caso->enviado_em->format('d/m/Y');
  }

  public function getStatusEnvioCaso2Attribute()
  {
    $caso = $this->casos()->categoria(2)->first();

    return is_null($caso->enviado_em) ?
            "<strong>NÃO ENVIADO</strong>" :
            "ENVIADO EM: ".$caso->enviado_em->format('d/m/Y');
  }

  public function getStatusEnvioCaso3Attribute()
  {
    $casoCat3 = $this->casos()->categoria(3)->first();
    $casoCat4 = $this->casos()->categoria(4)->first();

    if (is_null($casoCat3->enviado_em) && is_null($casoCat4->enviado_em)) {
      return "<strong>NÃO ENVIADO</strong>";
    }

    if (!is_null($casoCat3->enviado_em)) {
      return "ENVIADO EM: ".$casoCat3->enviado_em->format('d/m/Y');
    }
    
    if (!is_null($casoCat4->enviado_em)) {
      return "ENVIADO EM: ".$casoCat4->enviado_em->format('d/m/Y');
    }
  }

  // SCOPES

  public function scopeAdmin($query)
  {
    return $query->where('tipo', 'admin');
  }

  public function scopeWithConsent($query)
  {
    return $query->where('check_termos', 1)
                 ->whereNotNull('check_termos_date')
                 ->orderBy('check_termos_date', 'DESC');
  }

  public function scopeAvaliadores($query)
  {
    return $query->where('tipo', 'avaliador');
  }

  public function scopeCoordenadores($query)
  {
    return $query->where('tipo', 'coordenador');
  }

  public function scopeFiltrarLocal($query, $cidade)
  {
    return $query->where('cidade', '!=', $cidade);
  }

  public function scopeAtivos($query)
  {
    return $query->whereNotNull('senha_criada_em');
  }

  public function scopeNaoIniciados($query)
  {
    return $query->whereNull('email_enviado_em');
  }

  public function scopeOrdenarPorMenosAvaliacoes($query)
  {
    return $query->withCount('avaliacoes')
                 ->orderBy('avaliacoes_count', 'asc');
  }

  // RELATIONSHIPS

  public function casos()
  {
    return $this->hasMany('RM\Models\Caso', 'coordenador_id');
  }

  public function avaliacoes()
  {
    return $this->hasMany('RM\Models\Avaliacao', 'avaliador_id');
  }

}
