<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo'); // coordenador / avaliador / admin
            $table->string('nome');
            $table->string('crm')->nullable();
            $table->string('cidade')->nullable();
            $table->string('estado', 2);

            $table->string('telefone')->nullable();
            $table->string('centro')->nullable();
            $table->string('grupo')->nullable();

            $table->string('email', 250)->unique();
            $table->string('password');

            $table->string('token_criacao_senha')->nullable();
            $table->datetime('email_enviado_em')->nullable();
            $table->datetime('senha_criada_em')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
