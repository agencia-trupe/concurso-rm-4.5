<?php

use Illuminate\Database\Seeder;

class CasosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('casos')->delete();

      $usuarios = DB::table('usuarios')->where('tipo', 'coordenador')->get();
      foreach($usuarios as $coordenador){
        // inserir 2 casos, 1 de cada categoria
        for ($c=1; $c <= 2; $c++) {
          DB::table('casos')->insert([
              'coordenador_id' => $coordenador->id,
              'categoria' => $c
          ]);
        }
      }
    }
}
