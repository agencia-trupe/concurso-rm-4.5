if(document.getElementById('app-vue-avaliador')){

  require('./components/linha-avaliacao.js');
  require('./components/linha-avaliacao-editavel.js');

  const app = new Vue({
    el: '#app-vue-avaliador',
    data() {
      return {
        isLoaded : false,
        avaliacoes: {
          novas: {
            categoria1: [],
            categoria2: [],
            categoria3: [],
            categoria4: []
          },
          historico: {
            categoria1: [],
            categoria2: [],
            categoria3: [],
            categoria4: []
          }
        }
      }
    },
    methods: {
      buscarAvaliacoes() {
        var t = this;
        t.isLoaded = false;
        axios.get('buscar-avaliacoes').then( response => t.atualizarAvaliacoes(response));
      },
      atualizarAvaliacoes(response){
        this.avaliacoes = response.data;
        this.isLoaded = true;
      }
    },
    mounted() {
      var t = this;
      t.buscarAvaliacoes();
    }
  });

}
