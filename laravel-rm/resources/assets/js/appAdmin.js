if(document.getElementById('app-vue-admin')){

  const app = new Vue({
    el: '#app-vue-admin',
    data() {
      return {
        isLoaded : false,
        disableAction : false,
        alerta : {
          mostrar : false,
          destino : '',
          tipo : '',
          codigo : ''
        }
      }
    },
    methods: {
      mostrarAlerta(msg, id, codigo){

        $('html, body').stop().animate({
          scrollTop: 0
        }, 300);

        this.alerta.mostrar = true;
        this.alerta.tipo = msg;
        this.alerta.codigo = codigo;

        if(msg == 'distribuir'){
          this.alerta.destino = 'distribuir-caso/' + codigo;
        }else if(msg == 'excluir'){
          this.alerta.destino = 'excluir-caso/' + codigo;
        }
      },
      fecharModal(){
        this.alerta.mostrar = false;
      },
      goToDestination(url){
        window.location.href = this.alerta.destino;
      }
    },
    mounted() {
      this.isLoaded = true;
    }
  });

}
