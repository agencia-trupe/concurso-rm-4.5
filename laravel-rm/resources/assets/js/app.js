
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

require('./appCoordenador');
require('./appAdmin');
require('./appAvaliador');

$(document).ready( function(){

  $(document).on('change', '#inputArquivo', function(){
    var placeholder = $('.input-arquivo .placeholder');
    var split = $('#inputArquivo').val().split("\\");
    var filename = split[split.length - 1];
    placeholder.html(filename);
  });

  if($('.auto-close').length){
    setTimeout( function(){
      $('.auto-close').fadeOut('normal');
    }, 2000);
  }

});
