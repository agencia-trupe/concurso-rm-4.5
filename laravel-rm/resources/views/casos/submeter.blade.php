@extends('template.index')

@section('conteudo')
<div id="app-vue">
  <div class="conteudo conteudo-submeter-caso com-recuoo">
    <div class="centralizar">

      @if(session('sucesso'))
        <p class="alerta alerta-sucesso auto-close">
          {{session('sucesso')}}
        </p>
      @endif

      <p class="small">
        Os trabalhos deverão ser desenvolvidos no arquivo modelo conforme
        estrutura indicada e ser submetidos exclusivamente pelo website
        <a href="{{env('SITE_DOMAIN')}}">{{env('SITE_DOMAIN')}}</a> até as
        23h59 min do dia {{env('SITE_DATA_MAX_ENVIO_EXTENSO')}}.
        Não serão aceitos relatos de caso que não utilizem o arquivo modelo ou
        que tenham sido submetidos após o horário e data limites.
      </p>

      <a href="download-formulario" class="download-link" title="Download do formulário">
        <span>
          <img src="images/icone_download.png" alt="Download">
        </span>
        DOWNLOAD DO FORMULÁRIO
      </a>

      <p>
        <strong>Envie aqui seu caso clínico.</strong>
        <br>
        <strong>IMPORTANTE:</strong> faça o download do formulário, preencha e salve em PDF, que
        deve ser o formato enviado.
      </p>

      <div class="botoes-envio">

        <div class="box {{!$casoCat1->isEnviado && $casoCat1->podeSerEnviado ? 'ativo' : 'inativo'}}">
          <a href="#" title="Enviar caso clínico" id="mostrarFormCat1" @click.prevent="mostrarForm(1, '{{!$casoCat1->isEnviado && $casoCat1->podeSerEnviado ? 1 : 0}}')">
            <h3>CATEGORIA 1</h3>
            <h2>INTOLERÂNCIA</h2>
            <p>
              Caso clínico de paciente com LMC que apresentou qualquer tipo de intolerância ao imatinibe e que esteja em uso de Tasigna como segunda linha de tratamento e apresente RM4,5 em exame de PCR em tempo real com escala internacional.
            </p>
            <div class="botao">
              @if($casoCat1->isEnviado)
                <span class="light">
                  CASO CLÍNICO ENVIADO EM: {{$casoCat1->enviado_em->format('d/m/Y')}}.
                </span>
              @else
                @if($casoCat1->podeSerEnviado)
                  <span>
                    <img src="images/icone-enviar.png" alt="Enviar arquivo">
                  </span>
                  <span>
                    ENVIAR CASO CLÍNICO
                  </span>
                @else
                  <span>
                    O prazo máximo para o envio de casos expirou em <strong>{{env('SITE_DATA_MAX_ENVIO')}}</strong>.
                  </span>
                @endif
              @endif                
            </div>
          </a>
        </div>

        <div class="box {{!$casoCat2->isEnviado && $casoCat2->podeSerEnviado ? 'ativo' : 'inativo'}}">
          <a href="#" title="Enviar caso clínico" id="mostrarFormCat2" @click.prevent="mostrarForm(2, '{{!$casoCat2->isEnviado && $casoCat2->podeSerEnviado ? 1 : 0}}')">
            <h3>CATEGORIA 2</h3>
            <h2>FALHA</h2>
            <p>
              Caso clínico de paciente com LMC que apresentou resistência primária ou perda de resposta ao imatinibe em uso de Tasigna como segunda linha de tratamento e apresente RM4,5 em exame de PCR em tempo real com escala internacional.
            </p>
            <div class="botao">
              @if($casoCat2->isEnviado)
                <span class="light">
                  CASO CLÍNICO ENVIADO EM: {{$casoCat2->enviado_em->format('d/m/Y')}}.
                </span>
              @else
                @if($casoCat2->podeSerEnviado)
                  <span>
                    <img src="images/icone-enviar.png" alt="Enviar arquivo">
                  </span>
                  <span>
                    ENVIAR CASO CLÍNICO
                  </span>
                @else
                  <span>
                    O prazo máximo para o envio de casos expirou em <strong>{{env('SITE_DATA_MAX_ENVIO')}}</strong>.
                  </span>
                @endif
              @endif                
            </div>
          </a>
        </div>

        <div class="box {{!$casoCat3->isEnviado && $casoCat3->podeSerEnviado && !$casoCat4->isEnviado ? 'ativo' : 'inativo'}}">
          <a href="#" title="Enviar caso clínico" id="mostrarFormCat3" @click.prevent="mostrarForm(3, '{{!$casoCat3->isEnviado && $casoCat3->podeSerEnviado && !$casoCat4->isEnviado ? 1 : 0}}')">
            <h3>CATEGORIA 3</h3>
            <h2>EARLY SWITCH</h2>
            <p>
              Caso clínico de paciente com LMC em uso de nilotinibe após troca precoce ao uso de imatinibe (definida como troca aos 3 meses por qualquer causa), que esteja em uso de Tasigna e com RM 4,5 em exame de PCR em tempo real com escala internacional
            </p>
            @if(!$casoCat4->isEnviado)
              <div class="botao">
                @if($casoCat3->isEnviado)
                  <span class="light">
                    CASO CLÍNICO ENVIADO EM: {{$casoCat3->enviado_em->format('d/m/Y')}}.
                  </span>
                @else
                  @if($casoCat3->podeSerEnviado)
                    <span>
                      <img src="images/icone-enviar.png" alt="Enviar arquivo">
                    </span>
                    <span>
                      ENVIAR CASO CLÍNICO
                    </span>
                  @else
                    <span>
                      O prazo máximo para o envio de casos expirou em <strong>{{env('SITE_DATA_MAX_ENVIO')}}</strong>.
                    </span>
                  @endif
                @endif
              </div>
            @endif
          </a>
        </div>

        <div class="box {{!$casoCat4->isEnviado && $casoCat4->podeSerEnviado && !$casoCat3->isEnviado ? 'ativo' : 'inativo'}} @if($casoCat3->isEnviado) {{'reduzido'}} @endif">
          <a href="#" title="Enviar caso clínico" id="mostrarFormCat4" @click.prevent="mostrarForm(4, '{{!$casoCat4->isEnviado && $casoCat4->podeSerEnviado && !$casoCat3->isEnviado ? 1 : 0}}')">
            <h2>PRIMEIRA LINHA</h2>
            <p>
              Caso clínico de paciente de LMC em uso de Tasigna em primeira linha de tratamento e apresente RM 4,5 em exame de PCR em tempo real com escala internacional
            </p>
            @if(!$casoCat3->isEnviado)
              <div class="botao">
                @if($casoCat4->isEnviado)
                  <span class="light">
                    CASO CLÍNICO ENVIADO EM: {{$casoCat4->enviado_em->format('d/m/Y')}}.
                  </span>
                @else
                  @if($casoCat4->podeSerEnviado)
                    <span>
                      <img src="images/icone-enviar.png" alt="Enviar arquivo">
                    </span>
                    <span>
                      ENVIAR CASO CLÍNICO
                    </span>
                  @else
                    <span>
                      O prazo máximo para o envio de casos expirou em <strong>{{env('SITE_DATA_MAX_ENVIO')}}</strong>.
                    </span>
                  @endif
                @endif
              </div>
            @endif
          </a>
        </div>

      </div>

    </div>
  </div>

  <transition name="fade">
    <div class="modal" v-show="form.mostrar" v-cloak>
      <div class="backdrop" @click.stop="fecharModal"></div>
      <div class="modal-conteudo">
        <form class="formCaso" action="{{route('submeter-caso')}}" method="post" enctype="multipart/form-data">
          {!! csrf_field() !!}

          @if($errors->any())
            <p class="alerta alerta-erro" id="retorno-submissao">
              {{$errors->first()}}
            </p>
          @endif

          <input type="hidden" name="_categoria" :value="form.categoria">
          <input type="hidden" name="_categoria_old" value="{{old('_categoria')}}">

          <div class="input-arquivo">
            <label for="inputArquivo">
              <span class="placeholder">
                [ENVIAR ARQUIVO - <strong>FORMATO PDF</strong>]
              </span>
              <span class="botao">Selecionar arquivo</span>
            </label>
            <input type="file" name="arquivo" required id="inputArquivo">
          </div>
          <input type="text" name="autor_principal" placeholder="Autor Principal" value="{{old('autor_principal')}}" required>
          <p>
            Garantir que o autor principal do relato de caso seja o médico que
             acompanha o paciente. O autor principal deve ser Hematologista/Oncologista.
             Em caso de mais de um médico acompanhando o paciente, um deles deve
             ser apontado como autor principal.
          </p>
          <input type="text" name="co_autor_1" placeholder="Co-autor 1 (se houver)" value="{{old('co_autor_1')}}">
          <input type="text" name="co_autor_2" placeholder="Co-autor 2 (se houver)" value="{{old('co_autor_2')}}">
          <input type="text" name="co_autor_3" placeholder="Co-autor 3 (se houver)" value="{{old('co_autor_3')}}">
          <input type="text" name="co_autor_4" placeholder="Co-autor 4 (se houver)" value="{{old('co_autor_4')}}">
          <input type="text" name="co_autor_5" placeholder="Co-autor 5 (se houver)" value="{{old('co_autor_5')}}">
          <input type="text" name="co_autor_6" placeholder="Co-autor 6 (se houver)" value="{{old('co_autor_6')}}">
          <input type="text" name="co_autor_7" placeholder="Co-autor 7 (se houver)" value="{{old('co_autor_7')}}">
          <input type="submit" value="ENVIAR" @click.once="">
        </form>
      </div>
    </div>
  </transition>

</div>
@endsection
