@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-admin com-recuoo">
    <div class="centralizar">

      <h2><span>RANKING DE CASOS CLÍNICOS</span></h2>

      @if($publicarRanking)

        <p class="titulo-categoria">
          CATEGORIA 1
        </p>
        <div class="lista-ranking">
          @forelse($rankingCat1 as $caso)

            <div class="linha">
              <div class="ranking-item ranking-contador"><span></span></div>
              <div class="ranking-item ranking-codigo" data-label="CLASSIFICAÇÃO/CÓDIGO">
                <span>
                  {{$caso->codigo}}
                </span>
              </div>
              <div class="ranking-item ranking-autor" data-label="NOME DO GRUPO">
                <span>
                  {{$caso->grupo}}
                </span>
              </div>
              <div class="ranking-item ranking-coautores" data-label="AUTOR E CO-AUTORES">
                  <p>
                    {{$caso->autor}}<br>
                    @if($caso->coautor_1)
                      {{$caso->coautor_1}}<br>
                    @endif
                    @if($caso->coautor_2)
                      {{$caso->coautor_2}}<br>
                    @endif
                    @if($caso->coautor_3)
                      {{$caso->coautor_3}}<br>
                    @endif
                    @if($caso->coautor_4)
                      {{$caso->coautor_4}}<br>
                    @endif
                    @if($caso->coautor_5)
                      {{$caso->coautor_5}}<br>
                    @endif
                    @if($caso->coautor_6)
                      {{$caso->coautor_6}}<br>
                    @endif
                    @if($caso->coautor_7)
                      {{$caso->coautor_7}}<br>
                    @endif
                  </p>                
              </div>
              <div class="ranking-item ranking-coordenador" data-label="NOME DO COORDENADOR">
                <span>
                  {{$caso->coordenador}}
                </span>
              </div>
              <div class="ranking-item ranking-pontos" data-label="TOTAL DE PONTOS">
                <span>
                  {{number_format((float)$caso->ranking, 2, ',', '.')}}
                </span>
              </div>
            </div>

          @empty

            <div class="linha">
              <div class="nenhum">
                Nenhum caso com pontuação
              </div>
            </div>

          @endforelse
        </div>

        <p class="titulo-categoria">
          CATEGORIA 2
        </p>
        <div class="lista-ranking">
          @forelse($rankingCat2 as $caso)

            <div class="linha">
              <div class="ranking-item ranking-contador"><span></span></div>
              <div class="ranking-item ranking-codigo" data-label="CLASSIFICAÇÃO/CÓDIGO">
                <span>
                  {{$caso->codigo}}
                </span>
              </div>
              <div class="ranking-item ranking-autor" data-label="NOME DO GRUPO">
                <span>
                  {{$caso->grupo}}
                </span>
              </div>
              <div class="ranking-item ranking-coautores" data-label="AUTOR E CO-AUTORES">
                  <p>
                    {{$caso->autor}}<br>
                    @if($caso->coautor_1)
                      {{$caso->coautor_1}}<br>
                    @endif
                    @if($caso->coautor_2)
                      {{$caso->coautor_2}}<br>
                    @endif
                    @if($caso->coautor_3)
                      {{$caso->coautor_3}}<br>
                    @endif
                    @if($caso->coautor_4)
                      {{$caso->coautor_4}}<br>
                    @endif
                    @if($caso->coautor_5)
                      {{$caso->coautor_5}}<br>
                    @endif
                    @if($caso->coautor_6)
                      {{$caso->coautor_6}}<br>
                    @endif
                    @if($caso->coautor_7)
                      {{$caso->coautor_7}}<br>
                    @endif
                  </p>
              </div>
              <div class="ranking-item ranking-coordenador" data-label="NOME DO COORDENADOR">
                <span>
                  {{$caso->coordenador}}
                </span>
              </div>
              <div class="ranking-item ranking-pontos" data-label="TOTAL DE PONTOS">
                <span>
                  {{number_format((float)$caso->ranking, 2, ',', '.')}}
                </span>
              </div>
            </div>

          @empty

            <div class="linha">
              <div class="nenhum">
                Nenhum caso com pontuação
              </div>
            </div>

          @endforelse
        </div>

        <p class="titulo-categoria">
          CATEGORIA 3 OU PRIMEIRA LINHA
        </p>
        <div class="lista-ranking">
          @forelse($rankingCat3 as $caso)

            <div class="linha">
              <div class="ranking-item ranking-contador"><span></span></div>
              <div class="ranking-item ranking-codigo" data-label="CLASSIFICAÇÃO/CÓDIGO">
                <span>
                  {{$caso->codigo}}
                </span>
              </div>
              <div class="ranking-item ranking-autor" data-label="NOME DO GRUPO">
                <span>
                  {{$caso->autor}}
                </span>
              </div>
              <div class="ranking-item ranking-coautores" data-label="AUTOR E CO-AUTORES">
                  <p>
                    {{$caso->autor}}<br>
                    @if($caso->coautor_1)
                      {{$caso->coautor_1}}<br>
                    @endif
                    @if($caso->coautor_2)
                      {{$caso->coautor_2}}<br>
                    @endif
                    @if($caso->coautor_3)
                      {{$caso->coautor_3}}<br>
                    @endif
                    @if($caso->coautor_4)
                      {{$caso->coautor_4}}<br>
                    @endif
                    @if($caso->coautor_5)
                      {{$caso->coautor_5}}<br>
                    @endif
                    @if($caso->coautor_6)
                      {{$caso->coautor_6}}<br>
                    @endif
                    @if($caso->coautor_7)
                      {{$caso->coautor_7}}<br>
                    @endif
                  </p>
              </div>
              <div class="ranking-item ranking-coordenador" data-label="NOME DO COORDENADOR">
                <span>
                  {{$caso->coordenador}}
                </span>
              </div>
              <div class="ranking-item ranking-pontos" data-label="TOTAL DE PONTOS">
                <span>
                  {{number_format((float)$caso->ranking, 2, ',', '.')}}
                </span>
              </div>
            </div>

          @empty

            <div class="linha">
              <div class="nenhum">
                Nenhum caso com pontuação
              </div>
            </div>

          @endforelse
        </div>

      @else

        <p class="destaque">
          O Ranking estará disponível em <strong>{{env('SITE_PUBLICACAO_RANKING')}}</strong>.
        </p>

      @endif

    </div>
  </div>

@endsection
