@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-admin com-recuoo">
    <div class="centralizar">

      <h2><span>Log de Consentimento de Usuário</span></h2>

      <table class="lista-usuarios">
        <thead>
          <tr>
            <th>Nome</th>
            <th>E-mail</th>
            <th>Tipo</th>
            <th>Data de Cadastro</th>
            <th>Data de Ativação de Conta</th>
            <th>Data de Aceite</th>
          </tr>
        </thead>
        <tbody>
          @foreach($usuarios as $u)
            <tr>
              <td>{{$u->nome}}</td>
              <td>{{$u->email}}</td>
              <td>{{$u->tipoExtenso}}</td>
              <td class='status'>
                <strong class="verde">{{$u->created_at->format('d/m/Y H:i:s')}}</strong>
              </td>
              <td class='status'>
                <strong class="verde">{{$u->senha_criada_em->format('d/m/Y H:i:s')}}</strong>
              </td>
              <td class='status'>
                <strong class="verde">{{$u->check_termos_date->format('d/m/Y H:i:s')}}</strong>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>


    </div>
  </div>

@endsection
