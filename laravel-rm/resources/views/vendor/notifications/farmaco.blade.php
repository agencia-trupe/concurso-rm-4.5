@component('mail::message')
# Novo caso clínico

## {{$titulo}}

Um novo arquivo foi enviado para revisão do administrador do Concurso RM4,5 de Casos Clínicos. Segue anexo cópia do arquivo.

Atenciosamente,<br>
Comissão do Concurso RM4,5
@endcomponent
