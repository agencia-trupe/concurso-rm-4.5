@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-cronograma">
    <div class="centralizar">

      <div class="coluna coluna-37">
        <h1>CRONOGRAMA</h1>
        <img src="images/cronograma.png" alt="Cronograma">
      </div>

      <div class="coluna coluna-63">
        <ul class="lista-cronograma">
          <li @if($faseAtual == '1') class="ativo" @endif >
            <strong>02 de fevereiro</strong>
            <span>Prazo para indicações dos médicos coordenadores pela área comercial Novartis</span>
          </li>
          <li @if($faseAtual == '2') class="ativo" @endif >
            <strong>Março</strong>
            <span>Prazo para validação e convite aos médicos coordenadores pela área médica Novartis</span>
          </li>
          <li @if($faseAtual == '3') class="ativo" @endif >
            <strong>17 de março</strong>
            <span>Reunião presencial com os coordenadores e membros da comissão julgadora para apresentação do programa</span>
          </li>
          <li @if($faseAtual == '4') class="ativo" @endif >
            <strong>De 19 de março a 17 de agosto</strong>
            <span>Reuniões dos grupos de trabalho para discussão dos casos clínicos</span>
          </li>
          <li>
            <strong>Até 17 de agosto</strong>
            <span>Deadline submissão dos casos clínicos para avaliação do comitê avaliador</span>
          </li>
          <li @if($faseAtual == '5') class="ativo" @endif >
            <strong>De 18 de agosto a 30 de setembro</strong>
            <span>Avaliação dos relatos de casos clínicos pelo comitê avaliador</span>
          </li>
          <li @if($faseAtual == '6') class="ativo" @endif >
            <strong>01 de novembro</strong>
            <span>Fórum de encerramento com divulgação dos vencedores e cerimônia de premiação</span>
          </li>
          <li @if($faseAtual == '7') class="ativo" @endif >
            <strong>2019</strong>
            <span>Premiação</span>
          </li>
        </ul>
      </div>

    </div>
  </div>

@endsection
