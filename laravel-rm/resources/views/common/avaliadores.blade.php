@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-avaliadores com-recuoo">
    <div class="centralizar">

      <div class="coluna coluna-37">
        <h1>AVALIADORES</h1>
        <img src="images/avaliacao.png" alt="Avaliadores">
      </div>

      <div class="coluna coluna-63">
        <div class="lista-avaliadores">
          
          <h3>
            Dra. Carla Boquimpani
          </h3>
          <h4>
              CRM: 60694
          </h4>
          <p>
            Possui Mestrado em clínica médica com foco em hematologia, graduação em Medicina pela Universidade Federal do Rio de Janeiro (1995). Possui residência em Clínica Médica e Hematologia e Hemoterapia. Atualmente é médica e pesquisadora do INSTITUTO ESTADUAL DE HEMATOLOGIA ARTHUR DE SIQUEIRA CAVALCANTI - HEMORIO.
          </p>

          <h3>
            Dra. Monika Conchon
          </h3>
          <h4>
              CRM: 64334
          </h4>
          <p>
            Médica (CRM: 64334) onco-hematologista, pertencente ao corpo clínico do Hospital Sirio Libanês de São Paulo (SP), sócia e reponsável médica do Laboratório inSitus Genética, Professora Colaboradora do Departamento de Hematologia e Hemoterapia do Hospital das Clínicas de São Paulo - FMUSP (SP) e médica responsável pelo Ambulatório Clínico e de Pesquisa em LMC (Leucemia Mielóide Crônica) do Hospital Santa Marcelina (SP). É graduada em Medicina pela Universidade Estadual de Londrina (1989), mestre em Hematologia e Hemoterapia pela Faculdade de Medicina da Universidade de São Paulo (1998) e doutora em Hematologia e Hemoterapia pela Faculdade de Medicina da Universidade de São Paulo (2004), com título de especialista em Hematologia e Hemoterapia pela Sociedade Brasileira de Hematologia e Hemoterapia (2007). Especializada em citogenética tumoral no Royal Marsdem Hospital Academic Departament of Hematology and Citogenetics, RMH, Inglaterra (1994 - 1996). Tem experiência na área de Medicina, com ênfase em Leucemia Mielóide Crônica e Citogenética de doenças oncohematológicas.
          </p>

          <h3>
            Dr. Brian Leber
          </h3>
          <p>
            O Dr. Brian Leber é um hematologista clínico e professor de medicina do McMaster University Medical Center e do Juravinski Cancer Center, com interesse na fisiopatologia da leucemia e em novos estudos sobre drogas. Como membro associado do Departamento de Bioquímica, ele colabora com o Dr. David Andrews em estudos que investigam mecanismos bioquímicos básicos no apoptose. Ele também é membro do Programa de Medicina Laboratorial Regional de Hamilton e, nesse cargo, é Diretor do Laboratório de Hematologia Molecular. Ele se formou na McGill Medical School, fez medicina interna na McGill e Hematology Training na McMaster, seguido por uma bolsa de pesquisa na Royal Hospital School of Medicine (Universidade de Londres, Reino Unido).
          </p>

          <h3>
            Dr. Giuseppe Saglio
          </h3>
          <p>
            Dr. Giuseppe Saglio é Professor de Medicina Interna e Hematologia na Universidade de Turim. Ele é diretor do Departamento de Ciências Clínicas e Biológicas da Universidade de Turim e responsável pela Divisão de Medicina Interna e Hematologia do Hospital Universitário de San Luigi. Ele se formou na Universidade de Turim em 1975. Desde então, ele estudou Medicina Interna na Universidade de Turim (1975-1980), Hematologia na Universidade de Milão (1980-1983) e Biologia Molecular na Universidade de Leiden (1976). Inserm-Creteil, Paris (1979) e Universidade da Califórnia (1983). É coordenador do programa de doutorado em Medicina Molecular e Terapia Experimental na Universidade de Turim, ex-presidente da Sociedade Italiana de Hematologia Experimental (SIES) e secretário geral do IACRLRD (Associação Internacional para Pesquisa Comparativa sobre Leucemia e Doenças Relacionadas). Ele publicou mais de 500 artigos revisados ​​por pares nos campos da patogênese molecular de malignidades hematológicas (1986-presente), medicina molecular aplicada à medicina clínica (1978-presente) e as bases moleculares da talassemia e hemoglobinopatias relacionadas (1978-1990).
          </p>
          
        </div>
      </div>

    </div>
  </div>

@endsection
