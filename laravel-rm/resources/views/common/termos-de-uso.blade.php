@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-regulamento @if(Auth::check()) com-recuoo @endif">
    <div class="centralizar">

      <h2>TERMOS DE USO</h2>

      <h2>1. Aceitação</h2>
      
      <p>
        Seu acesso e uso deste Site estão sujeitos aos seguintes termos e condições e a toda a legislação aplicável. Ao acessar e navegar por este Site, você aceita, sem limitação ou ressalva, estes Termos e Condições e reconhece que quaisquer outros acordos em relação ao uso desse Site entre você e a Novartis AG são substituídos e não tem valor ou efeito.
      </p>

      <h2>2. Informações/Condições Médicas</h2>

      <p>
        As informações do produto contidas neste site são disponibilizadas pela Novartis AG e se destinam exclusivamente a fins de informação geral. Muitos dos produtos farmacêuticos e dispositivos médicos listados estão disponíveis somente mediante prescrição de um médico ou profissional de saúde qualificado, e nem todos esses produtos podem estar disponíveis em todos os países. As informações do produto não são destinadas a fornecer informações médicas completas. CASO VOCÊ TENHA UMA CONDIÇÃO MÉDICA, PROCURE IMEDIATAMENTE SEU MÉDICO OU PROFISSIONAL DE SAÚDE. NÓS NÃO OFERECEMOS DIAGNÓSTICO MÉDICO PERSONALIZADO OU CONSELHOS DE TRATAMENTO ESPECÍFICOS PARAPACIENTES. Você deve sempre obter as informações médicas completas sobre seus medicamentos de venda sob prescrição ou dispositivos médicos (incluindo seus usos médicos benéficos e possíveis efeitos adversos), discutindo o uso apropriado de qualquer medicamento ou dispositivo médico diretamente com seu médico prescritor ou, quando apropriado, outro profissional de saúde. Os profissionais da área da saúde podem obter as informações médicas completas a partir da bula do produto. As informações sobre esses produtos podem variar de um país para outro. Os pacientes, médicos e outros profissionais de saúde devem verificar com as fontes de informações médicas e autoridades regulatórias para obter as informações apropriadas ao seu país. Adicionalmente, as regulamentações atuais em muitos países limitam (ou até mesmo proíbem, em alguns casos) a capacidade da Novartis AG de fornecer informações e/ou responder diretamente às questões do paciente sobre seus produtos de prescrição. Entretanto, a Novartis AG irá responder às perguntas e fornecer informações ao seu profissional de saúde qualificado em conformidade com as regulamentações locais.
      </p>

      <h2>3. Uso das informações</h2>

      <p>
        Você pode navegar livremente pelo site, mas você só pode acessar, baixar ou utilizar as informações desse site, incluindo textos, imagens, áudios e vídeos (as "Informações") para seu uso próprio não comercial. Você não pode distribuir, modificar, transmitir, reutilizar, republicar ou usar as Informações para fins comerciais, sem a permissão por escrito da Novartis AG. Você deve manter e reproduzir todo e qualquer aviso de direitos autorais ou outros avisos de direitos de propriedade contidos nas Informações que você baixar. Você deve presumir que tudo o que você vê ou lê nesse site está protegido por direito autoral, a menos que haja indicação expressa em contrário, e não pode ser utilizado, exceto como estabelecido nesses Termos e Condições ou no texto do Site, sem a permissão por escrito da Novartis AG. Exceto se de outra forma permitido nesse parágrafo, a Novartis AG não declara tampouco garante que seu uso dos materiais apresentados no Site não irá infringir os direitos de terceiros que não são de controlados ou afiliados da Novartis AG. Com exceção da autorização limitada acima exposta, nenhuma licença ou direito às Informações, ou qualquer direito autoral da Novartis AG ou de qualquer outra parte é concedido ou conferido a você.
      </p>
      
      <h2>4. Marcas registradas/direitos de propriedade</h2>

      <p>
        Você deve presumir que todos os nomes de produtos que aparecem nesse Site, quer ou não apresentados em letras maiúsculas, itálico ou com o símbolo de marca registrada são marcas registradas da Novartis. Esse Site também pode conter ou fazer referência a patentes, informações proprietárias, tecnologias, produtos, processos ou outros direitos de propriedade da Novartis AG e/ou outras partes. Nenhuma licença ou direito sobre qualquer dessas marcas registradas, patentes, segredos comerciais, tecnologias, produtos, processos e outros direitos de propriedade da Novartis AG e/ou outras partes é concedido ou conferido a você. Todos os nomes de produtos publicados em itálico nesse Site são marcas registradas de propriedade do Grupo Novartis ou licenciadas para o mesmo.
      </p>

      <h2>5. Exclusão de garantias</h2>

      <p>
        Embora a Novartis AG utilize esforços razoáveis para garantir que as Informações sejam precisas e atualizadas, as mesmas podem conter imprecisões ou erros de digitação. A Novartis AG reserva-se o direito de fazer alterações, correções e/ou melhorias às Informações e aos produtos e programas descritos em tais Informações, a qualquer momento e sem aviso. A Novartis AG não dá garantias ou declarações quanto à precisão de quaisquer Informações. A Novartis AG não assume qualquer responsabilidade por erros ou omissões no conteúdo do Site. TODAS AS INFORMAÇÕES SÃO FORNECIDAS “NA FORMA EM QUE ESTÃO”. A NOVARTIS NÃO DÁ GARANTIAS SOBRE A INTEGRIDADE OU PRECISÃO DAS INFORMAÇÕES NESSE SITE OU SEUS POSSÍVEIS USOS. CONSEQUENTEMENTE, AS INFORMAÇÕES DEVEM SER CUIDADOSAMENTE AVALIADAS PELOS VISITANTES DO SITE. NEM A NOVARTIS AG, NEM QUALQUER OUTRA EMPRESA DO GRUPO NOVARTIS, NEM QUALQUER OUTRA PARTE ENVOLVIDA NA CRIAÇÃO, PRODUÇÃO OU ENTREGA DESSE SITE A VOCÊ SERÁ RESPONSÁVEL POR DANOS DIRETOS, ACIDENTAIS, CONSEQUENCIAIS, INDIRETOS OU PUNITIVOS DECORRENTES DO ACESSO, USO OU INCAPACIDADE DE USAR ESSE SITE, OU QUAISQUER ERROS OU OMISSÕES NO CONTEÚDO DO SITE. Algumas jurisdições não permitem a exclusão de garantias implícitas, de forma que a exclusão acima não se aplica a você. A Novartis AG também não assume qualquer responsabilidade, e não deve ser responsabilizada por quaisquer danos ou vírus que podem infectar seu computador ou outra propriedade decorrente de seu acesso ou uso das Informações. A Novartis AG reserva-se o direito de descontinuar esse Site a qualquer momento sem aviso e sem responsabilidade.
      </p>

      <h2>6. Informações que você nos fornece</h2>

      <p>
        Exceto pelas informações cobertas por nossa Política de Privacidade, qualquer comunicação ou material que você transmita ao Site por correspondência eletrônica ou de outra forma, incluindo dados, perguntas, comentários, sugestões ou algo do tipo, são e serão tratados como não proprietários e não confidenciais. Tudo o que você transmite ou publica se torna de propriedade da Novartis AG ou suas afiliadas e pode ser utilizado para qualquer finalidade, incluindo, mas não se limitando a, reprodução, divulgação, transmissão, publicação, radiodifusão e postagem. Além disso, a Novartis AG é livre para usar, sem compensação a você, quaisquer ideais, conceitos, conhecimentos ou técnicas contidas em qualquer comunicação que você envie ao Site para toda e qualquer finalidade, incluindo, mas não se limitando ao desenvolvimento, fabricação e comercialização de produtos utilizando essas informações.
      </p>

      <h2>7. Produtos mundiais</h2>

      <p>
        Este Site pode conter informações sobre produtos e serviços mundiais, dentre os quais nem todos estão disponíveis em todos os lugares. Uma referência a um produto ou serviço neste Site não implica que tal produto ou serviço esteja ou estará disponível em sua localidade. Os produtos referidos neste Site podem estar sujeitos a diferentes exigências regulatórias, dependendo do país de uso. Consequentemente, os visitantes podem ser notificados de que determinadas seções deste Site são destinadas apenas a certos tipos de usuários especializados ou apenas para audiências em alguns países. Você não deve inferir nenhum conteúdo deste Site como uma promoção ou propaganda para qualquer produto ou para o uso de qualquer produto que não esteja autorizado pelas leis e regulamentações de seu país de residência.
      </p>

      <h2>8. Aviso legal</h2>

      <p>
        Nada neste Site constitui um convite ou oferta para investir ou negociar os títulos, valores ou certificados de ações emitidos nos Estados Unidos da América (ADRs) da Novartis. Em particular, , os resultados e desenvolvimentos efetivos podem ser materialmente diferentes de qualquer previsão, opinião ou expectativa expressa neste Site e o desempenho passado dos preços de ações e títulos mobiliários não deve ser utilizado como referência para o seu desempenho futuro.
      </p>

      <h2>9. Links a este site</h2>

      <p>
        A Novartis AG não revisou qualquer conteúdo dos sites de terceiras partes que contém links para este Site e não é responsável pelo conteúdo de nenhuma dessas páginas fora do site ou quaisquer outros sites com links para esse Site. Se você quiser estabelecer um link do seu site para este Site, você pode fazer esse link apenas para a página inicial. Você não pode fazer um link para outras páginas nesse Site sem o consentimento prévio da Novartis AG. Da mesma forma, a citação ou uso de uma ou mais partes deste Site no site de quaisquer terceiras partes sem o consentimento por escrito é proibido.
      </p>

      <h2>10. Links para outros sites</h2>

      <p>
        Os links para sites de terceiras partes podem ser fornecidos para o interesse ou conveniência dos visitantes deste Site. Nos esforçaremos para lhe informar quando você estiver saindo desse Site de que os termos de uso e política de privacidade do site da terceira parte podem ser diferentes. Entretanto, a Novartis AG não assume responsabilidade por links de nosso site para outros, e particularmente, não nos responsabilizamos pela precisão ou legalidade do conteúdo dos mesmos. Não aceitamos responsabilidade decorrente de uma violação ou omissão nas políticas de privacidade de terceiras partes.
      </p>

      <h2>11. Publicações para este site</h2>

      <p>
        Embora a Novartis AG possa, de tempos em tempos, monitorar ou revisar as discussões, chats, publicações, transmissões, quadro de mensagens e coisas do tipo no Site, a Novartis AG não tem a obrigação de fazê-lo e não assume responsabilidade decorrente do conteúdo de qualquer desses locais, nem por qualquer erro, difamação, calúnia, injúria, omissão, falsidade, materiais promocionais, obscenidade, pornografia, profanidade, perigo, violação de privacidade ou imprecisão contidos nas informações nesses locais no Site. Você está proibido de publicar, postar ou transmitir através deste Site materiais ilegais, promocionais, ameaçadores, caluniosos, difamatórios, obscenos, escandalosos, inflamatórios, pornográficos ou profanos ou qualquer material que possa constituir ou encorajar uma conduta que pode ser considerada uma infração penal, gerar responsabilidade civil ou de outra forma violar qualquer legislação. A Novartis AG irá cooperar totalmente com autoridades de aplicação da lei ou ordem judicial solicitando ou direcionando a Novartis AG a divulgar a identidade de qualquer um que publique tais informações ou materiais.
      </p>

      <h2>12. Consequências</h2>

      <p>
        Caso se torne de nosso conhecimento que você violou qualquer um dos Termos e Condições contidos nessa Declaração Legal, podemos imediatamente tomar a ação corretiva apropriada, incluindo impedir que o usuário utilize os serviços oferecidos pela Novartis AG e remover quaisquer informações, dados e conteúdos colocados no Site pelo usuário, a qualquer momento e sem aviso. Se formos lesados por sua violação, podemos, a nosso critério, buscar a reparação destes danos junto a você.
      </p>

      <h2>13. Revisões</h2>

      <p>
        A Novartis AG pode, a qualquer momento, revisar estes Termos e Condições atualizando esta publicação. O seu acesso a este Site é regulamentado por estas revisões e, portanto, você deve periodicamente visitar essa página para consultar a versão mais atualizada dos Termos e Condições aos quais você deve seguir.
      </p>
      
    </div>
  </div>

@endsection
