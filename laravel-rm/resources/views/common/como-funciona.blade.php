@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-como-funciona">
    <div class="centralizar">
      
        <div class="como-funciona-item">
            <div class="coluna coluna-37">
                <h1>
                    <span>
                        O QUE É<br> O CONCURSO
                    </span>
                </h1>
            </div>
            <div class="coluna coluna-63">
                <p>
                    O CONCURSO RM 4,5 DE CASOS CLÍNICOS 2018 envolverá o envio de relatos de casos clínicos de pacientes em tratamento de Leucemia Mieloide Crônica (LMC) em uso de Tasigna com resposta RM 4,5 por grupos de trabalho organizados pela Novartis envolvendo médicos do Brasil.
                </p>
                <p>
                    O objetivo é difundir o conhecimento técnico e científico sobre o tratamento da LMC com uso de Tasigna e permitir o intercâmbio de boas práticas entre os hematologistas do Brasil.
                </p>
            </div>
        </div>

        <div class="como-funciona-item">
            <div class="coluna coluna-37">
                <h1>CATEGORIAS</h1>
                <img src="images/categorias.png" alt="Categorias">
            </div>
            <div class="coluna coluna-63">
                <h2>CATEGORIA 1</h2>
                <h3>INTOLERÂNCIA</h3>
                <p>
                    Caso clínico de paciente com LMC que apresentou qualquer tipo de intolerância ao imatinibe e que esteja em uso de Tasigna como segunda linha de tratamento e apresente RM4,5 em exame de PCR em tempo real com escala internacional;
                </p>
                <h2>CATEGORIA 2</h2>
                <h3>FALHA</h3>
                <p>
                    Caso clínico de paciente com LMC que apresentou resistência primária ou perda de resposta ao imatinibe em uso de Tasigna como segunda linha de tratamento e apresente RM4,5 em exame de PCR em tempo real com escala internacional;
                </p>
                <h2>CATEGORIA 3</h2>
                <h3>EARLY SWITCH</h3>
                <p>
                    Caso clínico de paciente com LMC em uso de nilotinibe após troca precoce ao uso de imatinibe (definida como troca aos 3 meses por qualquer causa), que esteja em uso de Tasigna e com RM 4,5 em exame de PCR em tempo real com escala internacional.
                </p>
                <h2>OU</h2>
                <h3>PRIMEIRA LINHA</h3>
                <p>
                    Caso clínico de paciente de LMC em uso de Tasigna em primeira linha de tratamento e apresente RM 4,5 em exame de PCR em tempo real com escala internacional.
                </p>
            </div>
        </div>

        <div class="como-funciona-item">
            <div class="coluna coluna-37">
                <h1>AVALIAÇÃO</h1>
                <img src="images/avaliacao.png" alt="Avaliação">
            </div>
            <div class="coluna coluna-63">
                <p>
                    <strong>O COMITÊ AVALIADOR SERÁ FORMADO POR 4 PARTICIPANTES</strong>, médicos Hematologistas líderes nacionais e internacionais, os quais atuarão com total independência em relação à Novartis.
                </p>

                <p>
                    <strong>AVALIADORES:</strong><br>
                    Dra. Carla Boquimpani, Dra. Monika Conchon, Dr. Brian Leber, Dr. Giuseppe Saglio
                </p>

                <p>
                    <strong>CRITÉRIOS DE AVALIAÇÃO:</strong><br>
                    1. Título<br>
                    2. Relato do caso clínico<br>
                    3. Revisão da literatura<br>
                    4. Discussão e conclusão<br>
                    5. Referência
                </p>
                
                <p>
                    <strong>OBSERVAÇÃO:</strong><br>
                    Os trabalhos não serão avaliados pelo(s) membro(s) do Comitê Avaliador da mesma cidade do grupo de trabalho, visando garantir imparcialidade na avaliação. Os casos serão avaliados de forma anônima, ou seja, o avaliador não terá informação do nome dos médicos, Hospital ou cidade do relato de caso a ser avaliado. Esse controle será possível através da tecnologia da plataforma digital onde os casos serão submetidos.
                </p>
                
                <p>
                    <strong>ESCOLHA DOS VENCEDORES:</strong><br>
                    Serão escolhidos 3 grupos vencedores do concurso, sendo eles:<br>
                    <strong>VENCEDOR 1:</strong> Caso com maior pontuação na categoria 1<br>
                    <strong>VENCEDOR 2:</strong> Caso com maior pontuação na categoria 2<br>
                    <strong>VENCEDOR 3:</strong> Caso com maior pontuação na categoria 3                
                </p>
            </div>
        </div>

        <div class="como-funciona-item">
            <div class="coluna coluna-37">
                <h1>PRÊMIO</h1>
                <img src="images/premio.png" alt="PRÊMIO">
            </div>
            <div class="coluna coluna-63">
                <p>
                    Os grupos vencedores (coordenador e demais membros do grupo) serão convidados a participar e apresentar o caso em um preceptorship na instituição de um dos avaliadores internacionais dentro do programa de educação continuada da Novartis, além de terem o caso clínico publicado em 2019 em material promocional desenvolvido pela empresa.
                </p>                
            </div>
        </div>

        <div class="como-funciona-item">
            <div class="coluna coluna-37">
                <h1>CRONOGRAMA</h1>
                <img src="images/cronograma.png" alt="Cronograma">
            </div>
            <div class="coluna coluna-63">
                <ul class="lista-cronograma">
                    <li>
                        <strong>02 de fevereiro</strong>
                        <span>Prazo para indicações dos médicos coordenadores pela área comercial Novartis</span>
                    </li>
                    <li>
                        <strong>Março</strong>
                        <span>Prazo para validação e convite aos médicos coordenadores pela área médica Novartis</span>
                    </li>
                    <li>
                        <strong>17 de março</strong>
                        <span>Reunião presencial com os coordenadores e membros da comissão julgadora para apresentação do programa</span>
                    </li>
                    <li>
                        <strong>De 19 de março a 17 de agosto</strong>
                        <span>Reuniões dos grupos de trabalho para discussão dos casos clínicos</span>
                    </li>
                    <li>
                        <strong>Até 17 de agosto</strong>
                        <span>Deadline submissão dos casos clínicos para avaliação do comitê avaliador</span>
                    </li>
                    <li>
                        <strong>De 18 de agosto a 30 de setembro</strong>
                        <span>Avaliação dos relatos de casos clínicos pelo comitê avaliador</span>
                    </li>
                    <li>
                        <strong>01 de novembro</strong>
                        <span>Fórum de encerramento com divulgação dos vencedores e cerimônia de premiação</span>
                    </li>
                    <li>
                        <strong>2019</strong>
                        <span>Premiação</span>
                    </li>
                </ul>
            </div>
        </div>

        <a href="files/9008_Regulamento_22.01.18.pdf" target="_blank" title="Download do Regulamento">FAÇA O DOWNLOAD DO REGULAMENTO COMPLETO AQUI &raquo;</a>

    </div>
  </div>

@endsection
