@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-home">
    <div class="centralizar">

      @if(session('sucesso_criacao_senha'))
        <p class="alerta alerta-sucesso" style="margin-bottom: 30px;">
          {{session('sucesso_criacao_senha')}}
        </p>
      @endif

      <div class="box-home">
        <p>
          O CONCURSO RM 4,5 DE CASOS CLÍNICOS 2018 envolverá o envio de relatos de casos clínicos de pacientes em tratamento de Leucemia Mieloide Crônica (LMC) em uso de Tasigna com resposta RM 4,5 por grupos de trabalho organizados pela Novartis envolvendo médicos do Brasil.
        </p>
        <p>
          O objetivo é difundir o conhecimento técnico e científico sobre o tratamento da LMC com uso de Tasigna e permitir o intercâmbio de boas práticas entre os hematologistas do Brasil.
        </p>

        <div class="mini-bula">
          <h3>TASIGNA</h3>
          <p>nilotinibe</p>
          <h3>VIA ORAL</h3>
          <p>
            <strong>Contraindicações:</strong> Tasigna é contraindicado em pacientes com
            hipersensibilidade ao nilotinibe ou a qualquer um dos excipientes.<br>
            <strong>Interações Medicamentosas:</strong> Evitar em pacientes tratados com
            medicamentos conhecidos por prolongar o intervalo QT e antiarrítmicos. O
            monitoramento e o ajuste de dose podem ser necessários para as drogas que
            são substratos da via CYP3A4.
          </p>
          <h3>Forma farmacêutica e apresentação:</h3>
          <p>
            Tasigna 150 mg - embalagens contendo 120 cápsulas.
            Tasigna 200 mg - embalagens contendo 112 cápsulas.
          </p>
          <p>
            <strong>Indicações:</strong> Tasigna é indicado para:<br>
            - o tratamento de pacientes adultos com leucemia mieloide crônica cromossomo Philadelphia positivo (LMC
            Ph+) em fase crônica recém-diagnosticada;<br>
            - o tratamento de pacientes adultos com leucemia mieloide crônica cromossomo Philadelphia positivo (LMC
            Ph +) em fase crônica ou em fase acelerada após falha ou intolerância a pelo menos uma terapia prévia,
            incluindo imatinibe.
          </p>
          <p>
            <strong>Contraindicações:</strong> Tasigna é contraindicado em pacientes com hipersensibilidade ao nilotinibe ou a
            qualquer um dos excipientes.
          </p>
          <p>
            <strong>Advertências e Precauções:</strong> O tratamento com Tasigna está associado à trombocitopenia, neutropenia e
            anemia, geralmente reversíveis e controláveis por suspensão temporária ou redução da dose de Tasigna .
            Devem ser realizados hemogramas completos a cada duas semanas durante os primeiros 2 meses e depois
            mensalmente, ou quando clinicamente indicado. Cuidado com pacientes que apresentem ou possam
            desenvolver prolongamento do intervalo QT corrigido (por exemplo, pacientes com hipocalemia,
            hipomagnesemia, síndrome do QT longo congênito, doença cardíaca não controlada ou significante, incluindo
            infarto do miocárdio recente, insuficiência cardíaca congestiva, angina instável ou bradicardia clinicamente
            significativa; pacientes que tomam medicamentos antiarrítmicos ou outros fármacos que possam levar ao
            prolongamento do intervalo QT). Recomenda-se a realização de ECG antes de iniciar o tratamento com
            Tasigna e deve ser repetido conforme indicado clinicamente. Hipocalemia ou hipomagnesemia devem ser
            corrigidas antes da administração de Tasigna . Casos incomuns (0,1 a 1%) de morte súbita foram relatados
            em estudos clínicos em pacientes com fatores de risco cardíaco significativos (incluindo anormalidades de
            repolarização ventricular) ou com comorbidades/medicações concomitantes (nenhum caso de morte súbita foi
            reportado no estudo de fase III com LMC Ph+-FC recém-diagnosticada). A taxa estimada de relatos
            espontâneos de morte súbita é 0,02% por paciente ao ano. Eventos cardiovasculares (doença oclusiva arterial
            periférica, doença cardíaca isquêmica e evento cerebrovascular isquêmico) foram relatados em um estudo de
            fase 3 randomizado em pacientes com leucemia mieloide crônica recém-diagnosticada e observados em
            relatos pós-comercialização. Se ocorrerem sinais ou sintomas agudos de eventos cardiovasculares, aconselhar
            os pacientes a procurar atendimento médico imediato. O estado cardiovascular dos pacientes deve ser
            avaliado e fatores de risco cardiovasculares devem ser monitorados e gerenciados ativamente durante o
            tratamento com Tasigna de acordo com as diretrizes padrão. O ganho de peso rápido e inesperado deve ser
            cuidadosamente investigado. Se sinais de retenção grave de líquidos aparecerem durante o tratamento com
            Tasigna_Minibulanilotinibe, a etiologia deve ser avaliada e os doentes tratados em conformidade. É recomendado que o perfil
            lipídico seja determinado antes do início do tratamento com Tasigna avaliado a cada 3 e 6 meses antes do
            início da terapia, e pelo menos anualmente durante a terapia crônica. Se for necessário o uso de um inibidor
            da HMG CoA redutase (um agente redutor de lipídio), veja o item “Interações medicamentosas” antes de
            iniciar o tratamento, tendo em vista que certos inibidores da HMG CoA redutase são metabolizados pela via
            CYP3A4. O nível de glicose deve ser avaliado antes do início do tratamento com Tasigna e monitorado
            durante o tratamento. Se os resultados indicarem necessidade de terapia, o médico deverá seguir o tratamento
            conforme prática clínica. Teste para infecção da hepatite B antes do início do tratamento com Tasigna .
            Especialistas devem ser consultados antes do tratamento ser iniciado em pacientes com sorologia positiva
            para hepatite B (incluindo àqueles com doença ativa) e para pacientes cujo teste foi positivo para infecção da
            hepatite B durante o tratamento. Monitore cuidadosamente os portadores do vírus da hepatite B para sinais e
            sintomas de infecção ativa da hepatite B durante o tratamento e por vários meses após a descontinuação do
            tratamento. Cuidado em pacientes com insuficiência hepática. Cuidado em pacientes com histórico prévio de
            pancreatite. Interromper o tratamento em caso de elevação da lipase acompanhada de sintomas de dor
            abdominal. A biodisponibilidade do nilotinibe pode ser reduzida em pacientes com gastrectomia total.
            Devido à possível ocorrência da Síndrome de Lise Tumoral, correção da desidratação clinicamente
            significativa e tratamento de níveis elevados de ácido úrico são recomendados antes do uso de Tasigna . Não
            é recomendado em pacientes com problemas hereditários raros de intolerância à galactose, deficiência grave
            de lactase ou má absorção de glucose-galactose.
            Gravidez: mulheres com potencial para engravidar devem usar um método contraceptivo, durante o
            tratamento com Tasigna e, por até duas semanas após o término. Não deve ser utilizado durante a gravidez, a
            menos que seja claramente necessário. Amamentação: Mulheres que tomam Tasigna não devem amamentar
            durante o tratamento e por até 2 semanas após a última dose.
          </p>
          <p>
            <strong>Reações adversas: Muito comuns:</strong> dores de cabeça, náusea, constipação, diarreia, vomito, dor abdominal
            superior, rash (erupção cutânea), prurido, alopecia, pele seca, mialgia, artralgia, fadiga, mielossupressão
            (trombocitopenia, neutropenia, anemia), hipofosfatemia (incluindo diminuição de fósforo no sangue),
            hiperbilirrubinemia (incluindo aumento da bilirrubina sanguínea), aumento da alanina aminotransferase,
            aumento da aspartato aminotransferase, lipase aumentada, aumento do colesterol lipoprotéico (incluindo de
            baixa e alta densidade), aumento do colesterol total, aumento dos triglicérides sanguíneos, dor
            musculoesquelética após descontinuação do tratamento com Tasigna . <strong>Comuns:</strong> foliculite, infecção do trato
            respiratório superior (incluindo faringite, nasofaringite, rinite), papiloma cutâneo, leucopenia, eosinofilia,
            neutropenia febril, pancitopenia, linfopenia, anorexia, alterações hidroeletrolíticas (incluindo
            hipomagnesemia, hipercalemia, hipocalemia, hiponatremia, hipercalcemia, hipocalcemia, hiperfosfatemia),
            diabetes mellitus, hiperglicemia, hipercolesterolemia, hiperlipidemia, hipertrigliceridemia, diminuição de
            apetite, depressão, insônia, ansiedade, tonturas, neuropatia periférica, hipoestesia, parestesia, hemorragia
            ocular, edema periorbital, prurido ocular, conjuntivite, olhos secos (incluindo xeroftalmia), vertigens, angina
            pectoris, arritmia (incluindo bloqueio atrioventricular, flutter cardíaco, extra-sístoles, fibrilação atrial,
            taquicardia, bradicardia), palpitações, prolongamento do QT ao ECG, hipertensão, rubor, dispneia, dispneia
            aos esforços, epistaxe, tosse, disfonia, dor abdominal, pancreatite, desconforto abdominal/distensão,
            dispepsia, disgeusia, flatulência, alteração da função hepática, eritema, suores noturnos, eczema, urticária,
            hiperidrose, contusão, acne, dermatite (incluindo dermatite alérgica esfoliativa e acneiforme), espasmos
            musculares, dor óssea, dores nas extremidades, dor torácica, dor musculoesquelética, dor nas costas, dor no
            pescoço, dor no flanco, fraqueza muscular, polaciúria, astenia, edema periférico, pirexia, dor no peito
            (incluindo dor no peito não-cardíaca), dores, desconforto no peito, mal-estar, hemoglobina diminuída, amilase
            sanguínea aumentada, gama-glutamiltransferase aumentada, creatina fosfoquinase sanguínea aumentada,
            fosfatase alcalina sanguínea aumentada, insulina sanguínea aumentada, aumento de peso, diminuição de peso,
            diminuição das globulinas.<br>
            <strong>Incomum:</strong> pneumonia, infecção do trato urinário, gastroenterite, bronquite, infecção por herpes vírus,
            candidíase incluindo candidíase oral, hipertireoidismo, hipotireoidismo, gota, desidratação, apetite
            aumentado, dislipidemia, hemorragia intracraniana, acidente vascular cerebral isquêmico, ataque isquêmico
            transitório, infarto cerebral, enxaqueca, perda da consciência (incluindo síncope), tremores, distúrbios de
            atenção, hiperestesia, deficiência visual, visão borrada, diminuição da acuidade visual, edema palpebral,
            Tasigna_Minibulafotopsia, hiperemia (escleral, conjuntiva, ocular), olhos irritados, hemorragia conjuntiva, insuficiência
            cardíaca, infarto do miocárdio, doença arterial coronária, sopro cardíaco, derrames pleurais e pericárdicos,
            cianose, sopro cardíaco, crise hipertensiva, doença arterial oclusiva periférica, claudicação intermitente,
            estenose arterial de membros, hematoma, arteriosclerose, edema pulmonar, doença pulmonar intersticial, dor
            pleural, pleurisia, dor faringolaringea, irritação na garganta, hemorragia gastrintestinal, melena, ulceração na
            boca, refluxo gastresofágico, estomatite, dor esofágica, boca seca, gastrite, sensibilidade nos dentes,
            hepatotoxicidade, hepatite tóxica, icterícia, rash (erupção cutânea) esfoliativo, erupção, dor na pele,
            equimose, edema facial, rigidez musculoesquelética, , inchaço nas articulações, disúria, urgência para urinar,
            nocturia, dor nas mamas, ginecomastia, disfunção erétil, edema facial (incluindo tumefação da face), edema
            gravitacional, sintomas semelhantes a gripe, calafrios, mudança de temperatura corpórea (incluindo sensação
            de calor e sensação de frio), lactato desidrogenase sanguínea aumentada, ureia sanguínea aumentada.
            <strong>Frequência desconhecida:</strong> sepse, abscesso subcutâneo, abscesso anal, furúnculo, tinea pedis, papiloma oral,
            paraproteinemia, trombocitemia, leucocitose, hipersensibilidade, hiperparatiroidismo secundário, tiroidite,
            hiperuricemia, hipoglicemia, desorientação, confusão mental, amnésia, disforia, acidente cerebrovascular,
            estenose da artéria basilar, edema cerebral, neurite ótica, letargia, disestesia, síndrome das pernas inquietas,
            papiloedema, diplopia, fotofobia, inchaço ocular, blefarite, dor ocular, coriorretinopatia, conjuntivite alérgica,
            doença da superfície ocular, comprometimento auditivo, dor de ouvido, zumbido, disfunção ventricular,
            pericardite, diminuição da fração de ejeção, choque hemorrágico, hipotensão, trombose, estenose arterial
            periférica, hipertensão pulmonar, chiado no peito, dor orofaríngea, perfuração gastrintestinal ulcerativa,
            hemorragia retroperitoneal, hematêmese, ulcera gástrica, esofagite ulcerativa, sibilos, enterocolite,
            hemorroida, hérnia de hiato, hemorragia retal, gengivite, colestase, hepatomegalia, psoríase, eritema
            multiforme, eritema nodoso, úlcera cutânea, síndrome da eritrodisestesia palmo-plantar, petéquias,
            fotossensibilidade, bolhas, cisto dermóide, hiperplasia sebácea, atrofia da pele, descoloração da pele,
            esfoliação da pele, hiperpigmentação da pele, hipertrofia de pele, hiperqueratose, artrite, insuficiência renal,
            hematúria, incontinência urinaria, cromaturia, endurecimento da mama, menorragia, inchaço no mamilo,
            edema localizado, troponina aumentada, bilirrubina não-conjugada sanguínea aumentada, insulina sanguínea
            diminuída, insulina C-peptídeo diminuída, hormônio paratireoideano sanguíneo aumentado, Síndrome de Lise
            Tumoral e reativação da hepatite B.
          </p>
          <p>
            <strong>Interações medicamentosas:</strong> Evite em pacientes tratados com medicamentos conhecidos por prolongar o
            intervalo QT (por exemplo, cloroquina, metadona, halofantrina, claritromicina, haloperidol, moxifloxacina,
            bepridil, pimozida). Evite em pacientes tratados com medicamentos antiarrítmicos (por exemplo, amiodarona,
            disopiramida, procainamida, quinidina, sotalol). Evitar a administração de inibidores potentes do CYP3A4
            (por exemplo, cetoconazol, ritonavir, itraconazol, voriconazol, telitromicina). Cuidado com indutores do
            CYP3A4 (por exemplo, fenitoína, rifampicina, carbamazepina, fenobarbital, erva de São João). O uso
            concomitante de Tasigna com esomeprazol ou outros inibidores de bomba de próton não é recomendado.
            Tasigna pode ser usado concomitantemente com varfarina. Cuidado com os medicamentos que afetam a
            glicoproteína P. O nilotinibe é um inibidor moderado da CYP3A4. A exposição sistêmica de outros
            medicamentos metabolizados principalmente pela CYP3A4 (por exemplo, certos inibidores da HMG-CoA
            redutase) pode aumentar quando coadministrados com nilotinibe. O monitoramento e o ajuste de dose
            adequada podem ser necessários para as drogas que são substratos da CYP3A4 e que possuem um índice
            terapêutico estreito (por exemplo, alfentanil, ciclosporina, di-hidroergotamina, ergotamina, fentanil, sirolimus
            e tacrolimus), quando coadministradas com nilotinibe. Evite suco de grapefruit (toranja) e outros alimentos
            conhecidos por inibir a CYP3A4. Em uso simultâneo: os bloqueadores de H2 (por exemplo: famotidina)
            podem ser administrados aproximadamente 10 horas antes e aproximadamente 2 horas após a dose de
            Tasigna , antiácidos (por exemplo, hidróxido de alumínio, hidróxido de magnésio e simeticona) podem ser
            administrados aproximadamente 2 horas antes ou 2 horas após a dose de Tasigna.
          </p>
          <p>
            <strong>Posologia:</strong> Para tratamento de pacientes adultos com LMC Ph+-FC recém-diagnosticada: a dose
            recomendada de Tasigna é de 300 mg duas vezes ao dia. Para tratamento de pacientes adultos com LMC Ph +
            em FC ou FA após falha ou intolerância a pelo menos uma terapia prévia, incluindo imatinibe: a dose
            recomendada é de 400 mg duas vezes ao dia. Foram relatados aumento na glicose sanguínea e nos níveis de
            colesterol sérico no tratamento com Tasigna . Os níveis de glicose sanguínea e o perfil lipídico devem ser
            Tasigna_Minibulaavaliados antes de iniciar o tratamento com Tasigna® e monitorados durante o tratamento. Tasigna deve ser
            tomado 2 vezes ao dia, em intervalos de 12 horas, aproximadamente, e não deve ser ingerido com alimentos.
            Nenhum alimento deve ser consumido por, pelo menos, 2 horas antes e 1 hora após a tomada da dose. Para
            pacientes com dificuldade de deglutição, o conteúdo de cada cápsula deve ser disperso em uma colher de chá
            de suco de maçã e deve ser ingerido imediatamente. Não mais do que uma colher de chá de suco de maçã e
            nenhum outro alimento deve ser usado.
          </p>
          <p>
            <strong>USO ADULTO</strong><br>
            <strong>VENDA SOB PRESCRIÇÃO MÉDICA</strong><br>
            Reg. M.S. – 1.0068.1060<br>
            Informações completas para prescrição disponíveis mediante solicitação ao Departamento Médico da
            Novartis.<br>
            A PERSISTIREM OS SINTOMAS, O MÉDICO DEVERÁ SER CONSULTADO.<br>
            BSS 30.01.2018<br>
            2017-PSB/GLC-0916-s<br>
            Esta minibula foi atualizada em 19/02/2018
          </p>
        </div>

      </div>

    </div>
  </div>

@endsection
