@extends('template.index')

@section('conteudo')

  <div class="conteudo conteudo-regulamento com-recuoo">
    <div class="centralizar">

      <h1>REGULAMENTO</h1>
      <h2>CONCURSO RM4,5 DE RELATOS DE CASOS CLÍNICOS 2018</h2>
      
      <h3>I – DO PROGRAMA</h3>
      <p>
        Promovido pela Novartis Biociências S/A, o Concurso RM4,5 de Relatos de Casos Clínicos 2018 envolverá o envio de relatos de casos clínicos de pacientes em tratamento de Leucemia Mieloide Crônica (LMC) em uso de Nilotinibe por grupos de trabalho organizados pela Novartis envolvendo médicos do Brasil. Website exclusivo: www.rm45.com.br
      </p>
      
      <h3>II – DOS OBJETIVOS</h3>
      <p>
        a. Difundir o conhecimento técnico e científico sobre o tratamento da LMC com o uso de nilotinibe entre médicos Hematologistas e Oncologistas.
      </p>
      <p>
        b. Permitir o intercâmbio de boas práticas médicas dirigidas à área de hematologia entre médicos especialistas do Brasil.
      </p>

      <h3>III – DA ELEGIBILIDADE DOS PARTICIPANTES</h3>
      <p>
        Os participantes do concurso deverão atender aos seguintes critérios:
        <ul>
          <li>Ser médico e atuar no Brasil;</li>
          <li>Estar no painel de visitação dos consultores técnicos da Hematologia da Novartis.</li>
        </ul>
      </p>
      
      <p>
        Os participantes do concurso serão selecionados e convidados pela Novartis com base nos critérios acima após validação pela área médica. Serão formados grupos de trabalho de 4 a 7 médicos participantes, que terão encontros organizados pela área comercial Novartis para discussão de casos clínicos.
      </p>

      <p>
        Os participantes receberão as regras de participação antes da inscrição e assinarão termo de participação no primeiro encontro do grupo de trabalho, comprovando o cumprimento dos critérios de participação acima.
      </p>

      <p>
        Cada grupo de trabalho terá 1 coordenador (já considerado dentro do número de 4 a 7 médicos de cada grupo de trabalho). O coordenador será indicado pela área comercial Novartis e convidado oficialmente pela área médica Novartis e sujeitos à validação da Área Médica da Novartis, conforme processo descrito a seguir:
      </p>

      <p>
        Os coordenadores deverão seguir critérios de palestrante nacional de acordo com a política NP4 vigente (v18), com consequente validação de sua participação pela Área Médica Novartis.
      </p>
      
      <p>
        O coordenador de cada grupo terá as seguintes atribuições:
        <ul>
          <li>Liderar pelo menos 2 reuniões com o seu grupo de trabalho e ser responsável pelo engajamento dos membros do grupo</li>
          <li>Recolher o formulário de participação no Concurso de todos os membros do grupo e entregar para o participante Novartis na primeira reunião do grupo de trabalho</li>
          <li>Apresentar dados que demonstrem a importância do monitoramento molecular do paciente com LMC, a importância de se atingir dados de resposta de acordo com guidelines e dados sobre o uso de nilotinibe nesses pacientes. Para isso, deverá ser utilizado o slide kit padrão enviado pela área Médica Novartis.</li>
          <li>Esclarecer dúvidas dos membros do grupo</li>
          <li>Facilitar as discussões do grupo em relação aos casos clínicos</li>
          <li>Assegurar que os casos discutidos não envolvem indicações não aprovadas em bula</li>
          <li>Assegurar que os eventos adversos que possam ser identificados durante as apresentações dos casos clínicos serão relatados à Farmacovigilância da Novartis</li>
          <li>Garantir que os casos que serão submetidos ao Concurso possuem consentimento do paciente, por escrito e que informações pessoais como nome, registro hospitalar e data de nascimento estejam blindadas no caso de envio de arquivo com exames laboratoriais</li>
          <li>Garantir que o autor principal do relato de caso seja o médico que acompanha o paciente. O autor principal deve ser Hematologista ou Oncologista que atua na área de Hematologia. Em caso de mais de um médico acompanhando o paciente, um deles deve ser apontado como autor principal.</li>
          <li>Realizar a submissão dos relatos de casos clínicos escolhidos pelo grupo para o Concurso RM4,5 de relatos de casos clínicos, sendo obrigatório o envio de, no mínimo, um caso clínico do seu grupo, versões português e inglês.</li>
        </ul>
      </p>

      <p>
        O coordenador receberá honorário dentro da política Novartis para sua prestação de serviço dentro das atribuições relacionadas acima.
      </p>
      
      <h3>IV – DO COMITÊ AVALIADOR</h3>

      <p>
        O Comitê Avaliador será formado por 4 participantes, médicos Hematologistas líderes nacionais e internacionais selecionados pela área médica da Novartis, os quais atuarão com total independência em relação à Novartis. 
      </p>

      <p>
        Cada caso será avaliado por 3 membros do Comitê, selecionados aleatoriamente.
      </p>
      
      <p>
        Os critérios de avaliação serão (Pontuação total possível por avaliador: 100 pontos):
      </p>
      
      <p>
        1. Título 1-10 pontos (peso 1) / 2. Relato do caso clínico 1-10 pontos (peso 4) / 3. Revisão da literatura 1-10 pontos (peso 2) / 4. Discussão e conclusão 1-10 pontos (peso 3) / 5. Referências – sem pontuação
      </p>
      
      <p>
        Cada caso clínico pode somar até 300 pontos.
      </p>

      <p>
        Se existir empate após a conciliação das notas dos 4 avaliadores, os critérios de desempate seguirão a seguinte sequência:
      </p>

      <p>
        Maior pontuação obtida em Relato de caso clínico / Maior pontuação obtida em Discussão e conclusão / Maior pontuação em Revisão da literatura / Maior pontuação obtida em Título
      </p>
      
      <p>
        Os trabalhos não serão avaliados pelo(s) membro(s) do Comitê Avaliador da mesma cidade / município do autor principal do caso do grupo de trabalho, visando garantir imparcialidade na avaliação.
      </p>
      
      <p>
        Os casos serão avaliados de forma sigilosa – o avaliador não terá informação do nome dos médicos, Hospital ou cidade do relato de caso a ser avaliado.
      </p>
      
      <h3>V – DO TEMA</h3>

      <p>
        O concurso será destinado aos grupos de médicos pré-selecionados conforme item III deste regulamento, que participarem das reuniões de discussão de casos clínicos organizadas pela Novartis.
      </p>
      
      <p>
        Os casos clínicos deverão ser de pacientes em tratamento para LMC em uso de Nilotinibe que apresentem RM4,5 no momento da submissão do caso. Para isso, os casos clínicos serão avaliados em 3 categorias distintas:
      </p>

      <p>
        <i>Categoria 1:</i> Caso clínico de paciente com LMC que apresentou qualquer tipo de intolerância ao Imatinibe e que esteja em uso de nilotinibe como segunda linha de tratamento e apresente RM4,5 em exame de PCR em tempo real com escala internacional;
      </p>

      <p>
        <i>Categoria 2:</i> Caso clínico de paciente com LMC que apresentou resistência primária ou perda de resposta ao imatinibe em uso de nilotinibe como segunda linha de tratamento e apresente RM4,5 em exame de PCR em tempo real com escala internacional;
      </p>

      <p>
        <i>Categoria 3:</i> Caso clínico de paciente com LMC em uso de nilotinibe após troca precoce ao uso de Imatinibe (definida como troca aos 3 meses por qualquer causa) ou como primeira linha de tratamento e apresente RM4,5 em exame de PCR em tempo real com escala internacional;
      </p>

      <p>
        Além do motivo da troca, os casos deverão demonstrar o porquê da escolha do Nilotinibe (perfil de tolerabilidade, perfil do paciente, etc).
      </p>

      <p>
        Pacientes oriundos de estudo clínico não poderão ser submetidos ao concurso.
      </p>

      <p>
        Só serão aceitos casos clínicos em que o paciente estiver em uso de Nilotinibe no momento da submissão.
      </p>

      <h3>VI – DA PREPARAÇÃO E SUBMISSÃO DOS CASOS CLÍNICOS</h3>

      <p>
        A estrutura do caso clínico deverá respeitar os campos indicados no arquivo modelo enviado via e-mail aos coordenadores que forem participar das reuniões de discussão de casos clínicos organizadas pela Novartis e também disponível no website do Concurso.
      </p>

      <p>
        A estrutura para o relato de caso clínico compreende:
      </p>

      <p>
        1. Título / 2. Relato do caso clínico / 3. Revisão da literatura / 4. Discussão e conclusão / 5. Referências
      </p>

      <p>
        Os relatos de casos clínicos podem ter até 6 autores, sendo um indicado como autor principal, que deve ser o médico que acompanha o paciente em questão.
      </p>

      <p>
        Deve-se preservar a identidade do médico e do Serviço/Hospital onde o paciente do caso clínico foi atendido, não havendo nenhuma citação ou referência ao nome, local e/ou cidade no título e corpo do texto do relato de caso.
      </p>

      <p>
        É essencial preservar a identidade dos pacientes. Não usar o nome ou iniciais do paciente, omitir detalhes que possam identificar as pessoas, caso não sejam essenciais para o relato do caso.
      </p>

      <p>
        Necessário ter o consentimento, por escrito, do paciente para que seus dados e imagens sejam utilizados no relato de caso clínico submetido ao Concurso RM 4,5. Informações de pacientes deverão ser anonimizadas como forma de não ser possível a identificação do paciente.
      </p>

      <p>
        Não serão aceitos relatos de casos clínicos que considerem o uso de nilotinibe para indicações e posologia diferentes das de bula no país ou de pacientes que sejam oriundos de estudos clínicos
      </p>

      <p>
        É de responsabilidade dos autores a exatidão das referências bibliográficas utilizadas no trabalho.
      </p>

      <p>
        Os trabalhos deverão ser desenvolvidos no arquivo modelo conforme estrutura indicada e ser submetidos exclusivamente pelo website www.rm45.com.br até as 23h59 min do dia 17 de agosto de 2018. Não serão aceitos relatos de caso que não utilizem o arquivo modelo ou que tenham sido submetidos após o horário e data limites.
      </p>

      <p>
        Poderá ser submetido somente 01 (um) relato de caso de cada categoria por grupo de trabalho.
      </p>
      
      <p>
        O caso clínico deve ser enviado dentro da data limite: 17 de agosto de 2018.
      </p>
      
      <h3>VII – DA DINÂMICA DO CONCURSO E DIVULGAÇÃO DO RESULTADO</h3>

      <p>
        De acordo com os critérios definidos na sessão III, serão formados grupos de discussão de casos clínicos a partir de convite da área comercial Novartis. Cada grupo terá entre 4 a 7 membros, sendo um coordenador, que ficará responsável pelo alinhamento e coordenação dos trabalhos de seu grupo.
      </p>

      <p>
        Entre março e agosto de 2018, com suporte e organização da Novartis (locação de sala para evento, equipamentos, refeição, logística de participantes e honorário do coordenador), serão realizadas reuniões dos grupos de discussão para o debate sobre casos clínicos.
      </p>

      <p>
        Ao final das reuniões, cada grupo selecionará um relato de caso clínico de cada categoria que deverá ser submetido ao concurso RM 4,5.
      </p>

      <p>
        Consultores técnicos da Novartis estarão presentes nas reuniões, porém não terão qualquer ingerência ou interferência nas discussões dos grupos de trabalho, a não ser sob solicitação e caso possam colaborar com o grupo, dentro das limitações de informações dos presentes, considerando o cargo de cada consultor técnico Novartis presente na reunião.
      </p>

      <p>
        A área médica dará o suporte para o conteúdo de nilotinibe a ser apresentado. As aulas a serem apresentadas pelos coordenadores nos grupos de trabalho deverão utilizar o slide kit padrão enviado pela área Médica Novartis.
      </p>

      <p>
        Os casos serão avaliados de forma sigilosa através de ferramenta de votação, seguindo os critérios mencionados na sessão IV.
      </p>

      <p>
        Serão escolhidos 3 grupos vencedores do concurso, sendo eles:
      </p>
      
      <p>
        Vencedor 1: Caso com maior pontuação na categoria 1 / Vencedor 2: Caso com maior pontuação na categoria 2 / Vencedor 3: Caso com maior pontuação na categoria 3
      </p>
      
      <p>
        Vencedores serão divulgados na sessão da Novartis da Super Quinta durante o congresso de Hematologia de 2018 (HEMO 2018) a ser realizado em São Paulo no dia 01 de novembro de 2018.
      </p>

      <p>
        Os relatos de caso vencedores poderão ser publicados em material promocional Novartis. A assinatura do termo de participação formaliza o consentimento do autor.
      </p>

      <p>
        Além disso, os vencedores poderão ser convidados pela Novartis para apresentar seus relatos de casos clínicos em eventos Novartis.
      </p>

      <h3>VIII – DA PREMIAÇÃO</h3>

      <p>
        Os grupos vencedores (coordenador e demais membros do grupo) serão convidados a participar de um preceptorship na instituição de um dos avaliadores para educação e apresentação do caso, além de terem o caso clínico comentado publicado em 2019 em material promocional desenvolvido pela Novartis.
      </p>

      <p>
        O apoio educacional acima mencionado (exclusivamente para os membros e coordenadores dos grupos vencedores) é de caráter pessoal e intransferível para cada membro do grupo e não poderá ser convertido em dinheiro ou substituído por qualquer outra forma de compensação material. O apoio consistirá em inscrição para o evento, passagem aérea em classe econômica e/ou transporte terrestre em veículo automotor (dependendo da localização de residência) e hospedagem em apartamento Single, restringindo-se exclusivamente aos membros do grupo vencedor, não sendo extensivo a acompanhantes.
      </p>

      <p>
        Quaisquer despesas pessoais a mais incorridas pelos participantes que receberem o apoio educacional acima descrito durante sua participação no evento serão de única e exclusiva responsabilidade do participante.
      </p>

      <h3>IX – DO CRONOGRAMA*</h3>
      <ul>
        <li>
          02 de fevereiro: prazo para indicações dos médicos coordenadores pela área comercial Novartis
        </li>
        <li>
          Março: prazo para validação e convite aos médicos coordenadores pela área médica Novartis
        </li>
        <li>
          17 de março: reunião presencial com os coordenadores e membros da comissão julgadora para apresentação do programa
        </li>
        <li>
          De 19 de março a 17 de agosto: reuniões dos grupos de trabalho para discussão dos casos clínicos.
        </li>
        <li>
          Até 17 de agosto: deadline submissão dos casos clínicos para avaliação do comitê avaliador
        </li>
        <li>
          De 18 de agosto a 30 de setembro: avaliação dos relatos de casos clínicos pelo comitê avaliador
        </li>
        <li>
          01 de novembro: Fórum de encerramento com divulgação dos vencedores e cerimônia de premiação.
        </li>
        <li>
          2019: Premiação - publicação de material promocional com caso clínico comentado para cada grupo vencedor e apoio preceptorship para todos os integrantes dos grupos vencedores.
        </li>
      </ul>
      
      <p>
        *Datas poderão sofrer alteração mediante comunicação aos participantes
      </p>
      
      <h3>X – CONSIDERAÇÕES FINAIS</h3>

      <p>
        Ao inscreverem-se para o Concurso RM 4,5 de Relatos de Casos Clínicos 2018, os participantes tacitamente concordam com as regras constantes deste documento, bem como a utilização sem ônus e sem qualquer contrapartida pela Novartis, de seu respectivo nome, imagem e trabalho para divulgação em qualquer meio de comunicação e publicações científicas, em âmbito nacional ou internacional, conforme legislação vigente, assim como publicação em materiais científicos da Novartis, que se compromete a identificar a autoria do material divulgado.
      </p>

      <p>
        As opiniões ou declarações contidas nos relatos de casos clínicos são de responsabilidade única e exclusiva dos autores dos relatos e não refletem necessariamente a posição da Novartis. Suspeitas de conduta antiética entre os participantes do Concurso e na elaboração dos casos clínicos serão apreciadas pelo Comitê Avaliador e, se consideradas procedentes, acarretarão sua desclassificação. Os relatos de caso clínico que não estejam de acordo com este regulamento também poderão ser desclassificados do Concurso RM4,5. Não se estabelece, pela força dessa iniciativa, de caráter essencialmente científico e educacional, qualquer vínculo de qualquer natureza entre a Novartis e os participantes. A Novartis não será responsável por nenhuma despesa incorrida pelo participante deste Concurso para a elaboração e envio dos seus trabalhos ou por qualquer ato do participante que possa causar danos materiais ou morais a sua pessoa ou de terceiros. O Participante responderá integralmente pela veracidade e acuidade dos dados cadastrados e enviados, incluindo os dados constantes do relato de caso inscrito, incluindo as respectivas referências. A Novartis se reserva no direito de alterar este regulamento a qualquer tempo, mediante notificação prévia a todos os participantes. Quaisquer questões relativas a esta iniciativa de caráter científico e educacional que não estejam endereçadas no presente Regulamento, serão avaliadas pelo Comitê Avaliador, cuja decisão será soberana, não sendo admitido questionamento posterior.
      </p>

      <p>
        <i>Regulamento Versão 1.0 - São Paulo, 22 de janeiro de 2018 - Realização: NOVARTIS BIOCIÊNCIAS S/A</i>
      </p>

    </div>
  </div>

@endsection
