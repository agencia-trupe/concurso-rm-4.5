@if(Auth::check())
  <div class="faixa-usuario faixa-{{Auth::user()->tipo}}">
    <div class="centralizar">

      @if(Auth::user()->isAdmin)
        <a href="{{ url('/log-adesao') }}" class="btn btn-usuarios @if(str_is('usuarios*', Route::currentRouteName())) active @endif ">LOG</a>
      @endif

      <a href="{{ url('/logout') }}" class="btn btn-logout"
         onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
          SAIR [X]
      </a>

      <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
      </form>

      <span class="nome">
        @if(Auth::user()->isAdmin)
          <strong>ADMINISTRADOR</strong>
        @else
          OLÁ Dr(a). {{Auth::user()->nome}}
        @endif
      </span>

    </div>
  </div>
@endif
