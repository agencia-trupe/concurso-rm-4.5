@include('components.faixa-usuario')

<header>
  <div class="banner">
    <a href="{{route('home')}}" title="Página Inicial">
      <img src="images/cabecalho.jpg" alt="Concurso RM 4.5 Casos Clínicos">
    </a>
  </div>
  
  @include('partials.menu')
</header>
