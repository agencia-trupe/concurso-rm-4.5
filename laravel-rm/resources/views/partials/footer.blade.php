<footer>
  <div class="centralizar">

    <div class="bloco-footer bloco-infomec">
      <img src="images/infomec.png" alt="Infomec">
    </div>

    <div class="bloco-footer bloco-disclaimer">
      <p>
        Material destinado exclusivamente a profissionais de saúde habilitados a prescrever e/ou dispensar medicamentos.<br>
        {{date('Y')}} &copy; - Direitos Reservados - Novartis Biociências S.A.<br>
        Proibida a reprodução total ou parcial sem a autorização do titular.<br>
        Janeiro/2018.<br>
        0000000 TS CONVITE LMCARE 1,0 0517 BR NP4 000000
      </p>
    </div>
    
    <div class="bloco-footer bloco-novartis">
      <img src="images/novartis.png" alt="Novartis">
      <p>
        Novartis Biociências S.A.<br>
        Setor Farma - Av. Prof. Vicente Rao, 90<br>
        São Paulo, SP - CEP 04636-000<br>
        <a href="www.novartis.com.br" target="_blank" title="Novartis">www.novartis.com.br</a><br>
        <a href="www.portalnovartis.com.br" target="_blank" title="Portal Novartis">www.portalnovartis.com.br</a>
        <br><br>
        Infomec<br>
        Informações Médico Científicas<br>
        <a href="mailto:infomec.novartis@novartis.com" title="Enviar um e-mail">infomec.novartis@novartis.com</a><br>
        0800 888 3003 (fixo)<br>
        11 3253-3405 (cel)<br>
      </p>
    </div>

    <div class="bloco-footer bloco-termos">
      <p>
        SIC | Serviço de Informações ao Cliente <br> <a href="mailto:sic.novartis@novartis.com" title="Entrar em contato">sic.novartis@novartis.com</a>
        <br><br>
        O uso deste site é governado por nossos <br><a href="https://www.novartis.com.br/termos-de-uso" target="_blank" title="Termos de Uso">Termos de Uso</a> e nosso <a href="aviso-de-privacidade" title="Aviso de Privacidade">Aviso de Privacidade</a>.
        <br><br>
        &copy; {{date('Y')}} Novartis Biociências S.A.
      </p>
    </div>

  </div>
</footer>
